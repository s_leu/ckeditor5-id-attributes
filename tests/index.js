import { IdAttributes as IdAttributesDll, icons } from '../src';
import IdAttributes from '../src/idattributesui';

import ckeditor from './../theme/icons/ckeditor.svg';

describe( 'CKEditor5 IdAttributes DLL', () => {
	it( 'exports IdAttributes', () => {
		expect( IdAttributesDll ).to.equal( IdAttributes );
	} );

	describe( 'icons', () => {
		it( 'exports the "ckeditor" icon', () => {
			expect( icons.ckeditor ).to.equal( ckeditor );
		} );
	} );
} );
